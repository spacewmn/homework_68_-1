import React, {useEffect} from "react";
import {useSelector, useDispatch} from "react-redux";
import "./Counter.css";
import {
    addCounter,
    decrementCounter,
    fetchCounter,
    incrementCounter,
    postCounter,
    subtractCounter
} from "../../store/actions";

const Counter = () => {
  const counter = useSelector(state => state.counter);
  const disableBtn = useSelector(state => state.disabledButton)
  const dispatch = useDispatch();

  const incrementCounterHandler = () => {
      dispatch(incrementCounter());
  };
  const decrementCounterHandler = () => {
      dispatch(decrementCounter());
  };
  const addCounterHandler = (value) => {
      dispatch(addCounter(value));
  };
  const subtractCounterHandler = (value) => {
      dispatch(subtractCounter(value));
  };

  useEffect(() => {
    dispatch(fetchCounter());
  }, [dispatch]);

  useEffect(() => {
      dispatch(postCounter(counter));
  }, [dispatch, counter])

  return (
    <div className="Counter">
      <h1>{counter}</h1>
      <button onClick={incrementCounterHandler} disabled={disableBtn}>Increase</button>
      <button onClick={decrementCounterHandler} disabled={disableBtn}>Decrease</button>
      <button onClick={() => addCounterHandler(5)} disabled={disableBtn}>Increase by 5</button>
      <button onClick={() => subtractCounterHandler(5)} disabled={disableBtn}>Decrease by 5</button>
    </div>
  );
};

export default Counter;