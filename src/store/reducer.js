import {
  ADD,
  DECREMENT,
  FETCH_COUNTER_ERROR,
  FETCH_COUNTER_REQUEST,
  FETCH_COUNTER_SUCCESS,
  INCREMENT, POST_ERROR, POST_REQUEST, POST_SUCCESS,
  SUBTRACT
} from "./actionTypes";

const initialState = {
  counter: 0,
  disabledButton: false,
  error: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case INCREMENT:
      return {...state, counter: state.counter + 1};
    case DECREMENT:
      return {...state, counter: state.counter - 1};
    case ADD:
      return {...state, counter: state.counter + action.value};
    case SUBTRACT:
      return {...state, counter: state.counter - action.value};
    case FETCH_COUNTER_REQUEST:
      return {...state};
    case FETCH_COUNTER_SUCCESS:
      return {...state, counter: action.value};
    case FETCH_COUNTER_ERROR:
      return {...state, error: action.error};
    case POST_REQUEST:
      return {...state, disabledButton: true};
    case POST_SUCCESS:
      return {...state, disabledButton: false};
    case POST_ERROR:
      return {...state, disabledButton: false, error: action.error};
    default:
      return state;
  }
};

export default reducer;